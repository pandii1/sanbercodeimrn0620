var x = 2
var y = 0
var z = 22


console.log('NO 1 LOOPING WHILE')
console.log('LOOPING PERTAMA');
while(y < 20) {
    y += x;
    console.log(y + ' - I love coding');
}
console.log('LOOPING KEDUA');
while(z > 2) {
    z -=x;
    console.log(z + ' I will be a mobile developer')
}


console.log('NO 2 LOOPING FOR')
for(var angka = 1; angka <= 20;angka++) {
    if(angka%2 == 0) {
        console.log(angka + ' - Berkualitas');
    }else if(angka%3 == 0) {
        console.log(angka+' - I love coding');
    }else  
        console.log(angka + ' - Santai');
    
}


console.log('NO 3 MEMBUAT PERSEGI PANJANG');
var e = '';
for(var i = 0; i < 4; i++) {
    for(var j = 0; j < 8; j++) {
        e += '#';
    }
    e += "\n";
}
console.log(e);


console.log('NO 4 MEMBUAT TANGGA');
var e = '';
for(var i = 0; i < 8; i++) {
    for(var j = 0; j < i; j++) {
        e += '#';
    }
    e += "\n";
}
console.log(e);


console.log('NO 5 MEMBUAT PAPAN CATUR');

for(var i = 0; i < 8; i++) {
    var e = '';
    for(var j = 0; j < 4 ; j++) {
        if(i%2 === 0) {
            e += '# '
        }else {
            e += ' #'
        }
    }
    console.log(e);
}